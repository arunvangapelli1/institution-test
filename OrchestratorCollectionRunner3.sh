#/bin/bash

#import setup
. setup.sh $1

runOrchestratorCollection "ts-sv-api-orchestrator-reauthcapture"
runOrchestratorCollection "ts-sv-api-orchestrator-refund"
runOrchestratorCollection "ts-sv-api-orchestrator-fundreturn"
runOrchestratorCollection "ts-sv-api-orchestrator-rules-engine-failures"
#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
