#/bin/bash

#import setup
. setup.sh $1

runOrchestratorCollection "ts-sv-api-orchestrator-auth"
runOrchestratorCollection "ts-sv-api-orchestrator-authcapture"
runOrchestratorCollection "ts-sv-api-orchestrator-void"
#runOrchestratorCollection "ts-sv-api-orchestrator-validation"

#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
