package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@TestInstance(Lifecycle.PER_CLASS)
class DtiAuthTest extends DtiTestingBase {

    // txn amount needs to be 12 characters = $1.00
    private final String TXN_AMOUNT = "000000000100";

    @Test
    public void authHappyPathTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiAuthPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.AUTH_RES_MTI, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.AUTH_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_CH_BILLING));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }

    @Test
    public void authPosAdapterFailureTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiAuthPayloadConstructor(
                DtiPayload.builder().isoCredentialId("INVALIDPAN").isoTerminalKey(tResolver.getIso8583terminalNumber())
                        .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.AUTH_RES_MTI, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.AUTH_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals("INVALIDPAN", DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.FAILURE_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }

    @Test
    public void authInsufficientFundsFailureTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String superHighTxnAmount = "999999999999";
        String request = DtiUtility.dtiAuthPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(superHighTxnAmount).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.AUTH_RES_MTI, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.AUTH_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(superHighTxnAmount, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.FAILURE_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }
}
