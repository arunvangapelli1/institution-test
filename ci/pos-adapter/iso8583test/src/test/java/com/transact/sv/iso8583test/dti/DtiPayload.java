package com.transact.sv.iso8583test.dti;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DtiPayload {
    private String isoCredentialId;
    private String isoTerminalKey;
    private String isoMerchantKey;
    private String txnAmount;
    private String authCode;
}
