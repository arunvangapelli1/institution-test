package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@TestInstance(Lifecycle.PER_CLASS)
class DtiVoidTest extends DtiTestingBase {

    // txn amount needs to be 12 characters = $1.00
    private final String TXN_AMOUNT = "000000000100";

    @Test
    public void voidHappyPathTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String authRequest = DtiUtility.dtiAuthPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String authResponse = sendDtiPayloadThenGetResponse(authRequest);
        assertNotNull(authResponse);

        Document authResponseDoc = DtiUtility.transformResponse(authResponse);
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RESULT_CODE));

        String authCode = DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM);
        String voidRequest = DtiUtility.dtiVoidPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).authCode(authCode).build());
        String voidResponse = sendDtiPayloadThenGetResponse(voidRequest);
        assertNotNull(voidResponse);

        Document voidResponseDoc = DtiUtility.transformResponse(voidResponse);
        assertEquals(DtiUtility.VOID_RES_MTI,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.VOID_PROCESSING_CODE,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(authCode, DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }

    @Test
    public void voidPosAdapterNotFoundTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String authRequest = DtiUtility.dtiAuthPayloadConstructor(
                DtiPayload.builder().isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                        .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String authResponse = sendDtiPayloadThenGetResponse(authRequest);
        assertNotNull(authResponse);

        Document authResponseDoc = DtiUtility.transformResponse(authResponse);
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RESULT_CODE));

        String voidRequest = DtiUtility.dtiVoidPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT)
                .authCode("INVALID_AUTH_CODE").build());
        String voidResponse = sendDtiPayloadThenGetResponse(voidRequest);
        assertNotNull(voidResponse);

        Document voidResponseDoc = DtiUtility.transformResponse(voidResponse);
        assertEquals(DtiUtility.VOID_RES_MTI,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.VOID_PROCESSING_CODE,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals("INVALID_AUTH_CODE",
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.ORIGINAL_NOT_FOUND_RESULT_CODE,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
        assertEquals(DtiUtility.ORIGINAL_NOT_FOUND_ERROR_MSG,
                DtiUtility.getFieldValue(voidResponseDoc, DtiUtility.FIELD_DISPLAY_PRINT_DATA));
    }

}
