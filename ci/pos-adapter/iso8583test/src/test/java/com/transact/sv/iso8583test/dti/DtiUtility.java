package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.platform.commons.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DtiUtility {

    // iso 8583 field values
    public static final String FIELD_MESSAGE_TYPE_IND = "0";
    public static final String FIELD_PRIMARY_ACCOUNT_NUMBER = "2";
    public static final String FIELD_PROCESSING_CODE = "3";
    public static final String FIELD_AMOUNT_TRANSACTION = "4";
    public static final String FIELD_AMOUNT_SETTLEMENT = "5";
    public static final String FIELD_AMOUNT_CH_BILLING = "6";
    public static final String FIELD_TRANSMISSION_DATE_TIME = "7";
    public static final String FIELD_SYSTEM_TRACE_AUDIT_NUM = "11";
    public static final String TRANSACTION_TIME = "12";
    public static final String TRANSACTION_DATE = "13";
    public static final String FIELD_EXPIRATION_DATE = "14";
    public static final String FIELD_MERCHANT_TYPE = "18";
    public static final String POS_ENTRY_MODE = "22";
    public static final String POS_CONDITION_CODE = "25";
    public static final String FIELD_CREDENTIAL_ID = "35";
    public static final String FIELD_RETRIEVAL_REFERENCE_NUM = "37";
    public static final String FIELD_APPROVAL_CODE = "38";
    public static final String FIELD_RESULT_CODE = "39";
    public static final String FIELD_RESPONSE_CODE = "40";
    public static final String FIELD_CARD_ACCEPTOR_TERMINAL_ID = "41";
    public static final String FIELD_CARD_ACCEPTOR_ID = "42";
    public static final String FIELD_CURRENCY_CODE_TXN = "49";
    public static final String FIELD_TERMINAL_VERSION = "60";
    public static final String FIELD_BALANCE_INFORMATION = "62";
    public static final String FIELD_DISPLAY_PRINT_DATA = "63";

    // response result values
    public static final String SUCCESS_RESULT_CODE = "00";
    public static final String FAILURE_RESULT_CODE = "99";
    public static final String ORIGINAL_NOT_FOUND_RESULT_CODE = "16";
    public static final String ORIGINAL_NOT_FOUND_ERROR_MSG = "Original not found";

    private static final String TERM_VERSION = "TT0000TestTerminal";
    private static final String BAL_INFO = "00";
    private static final String SYS_TRACE_AUDIT_NUM = "000027";
    // auth fields
    private static final String AUTH_REQ_MTI = "0100";
    public static final String AUTH_RES_MTI = "0110";
    // auth/capture fields
    private static final String AUTH_CAPTURE_REQ_MTI = "0200";
    public static final String AUTH_CAPTURE_RES_MTI = "0210";
    public static final String AUTH_PROCESSING_CODE = "000000";
    // balance inquiry fields
    private static final String BALANCE_REQ_MTI = "0100";
    public static final String BALANCE_RES_MTI = "0110";
    public static final String BALANCE_PROCESSING_CODE = "310000";
    // fund fields
    private static final String FUND_REQ_MTI = "0200";
    public static final String FUND_RES_MTI = "0210";
    public static final String FUND_PROCESSING_CODE = "230000";
    // void fields
    private static final String VOID_REQ_MTI = "0200";
    public static final String VOID_RES_MTI = "0210";
    public static final String VOID_PROCESSING_CODE = "020000";
    // capture fields
    private static final String CAPTURE_REQ_MTI = "0220";
    public static final String CAPTURE_RES_MTI = "0230";
    public static final String CAPTURE_PROCESSING_CODE = "000000";

    private static final String ISO_START_MSG = "<isomsg direction=\"incoming\"><header>6000000000</header>";
    private static final String ISO_END_IND = "</isomsg>";
    private static final String ISO_FIELD = "<field id=\"%s\" value=\"%s\"/>";

    private static final String FIELD_VALUE_XPATH = "/isomsg/field[(@id='%s')]/@value";

    public static String dtiBalancePayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, BALANCE_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, BALANCE_PROCESSING_CODE));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }

    public static String dtiAuthPayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, AUTH_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, AUTH_PROCESSING_CODE));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }

    public static String dtiAuthCapturePayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, AUTH_CAPTURE_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, AUTH_PROCESSING_CODE));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }

    public static String dtiFundPayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, FUND_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, FUND_PROCESSING_CODE));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }

    public static String dtiVoidPayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, VOID_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, VOID_PROCESSING_CODE));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }
    
    public static String dtiCapturePayloadConstructor(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        sb.append(field(FIELD_MESSAGE_TYPE_IND, CAPTURE_REQ_MTI));
        sb.append(field(FIELD_PROCESSING_CODE, CAPTURE_PROCESSING_CODE));
        sb.append(field(TRANSACTION_TIME, "103000"));
        sb.append(field(TRANSACTION_DATE, "0405"));
        sb.append(field(POS_CONDITION_CODE, "00"));
        sb.append(field(FIELD_APPROVAL_CODE, "000000"));
        return genericDtiPayloadConstruction(sb.toString(), payloadDtiPayloadConstruction(payload));
    }

    private static String payloadDtiPayloadConstruction(DtiPayload payload) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(payload.getTxnAmount())) {
            sb.append(field(FIELD_AMOUNT_TRANSACTION, payload.getTxnAmount()));
        }
        if (StringUtils.isNotBlank(payload.getAuthCode())) {
            sb.append(field(FIELD_RETRIEVAL_REFERENCE_NUM, payload.getAuthCode()));
        }
        sb.append(field(FIELD_CREDENTIAL_ID, payload.getIsoCredentialId()));
        sb.append(field(FIELD_CARD_ACCEPTOR_TERMINAL_ID, payload.getIsoTerminalKey()));
        sb.append(field(FIELD_CARD_ACCEPTOR_ID, payload.getIsoMerchantKey()));
        return sb.toString();
    }

    private static String genericDtiPayloadConstruction(String txnSpecificFields, String payloadDetails) {
        StringBuilder sb = new StringBuilder();
        sb.append(ISO_START_MSG);
        sb.append(txnSpecificFields);
        sb.append(payloadDetails);
        sb.append(field(FIELD_SYSTEM_TRACE_AUDIT_NUM, SYS_TRACE_AUDIT_NUM));
        sb.append(field(FIELD_TERMINAL_VERSION, TERM_VERSION));
        sb.append(field(FIELD_BALANCE_INFORMATION, BAL_INFO));
        sb.append(field(FIELD_TRANSMISSION_DATE_TIME, "0211150700"));
        sb.append(field(FIELD_EXPIRATION_DATE, "1212"));
        sb.append(field(POS_ENTRY_MODE, "010"));
        sb.append(ISO_END_IND);
        return sb.toString();
    }

    private static String field(String id, String val) {
        return String.format(ISO_FIELD, id, val);
    }

    public static Document transformResponse(String response)
            throws SAXException, IOException, ParserConfigurationException {
        return DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(response)));
    }

    public static String getFieldValue(Document response, String id) throws XPathExpressionException {
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        List<String> values = new ArrayList<>();
        XPathExpression expr = xpath.compile(String.format(FIELD_VALUE_XPATH, id));
        NodeList nodes = (NodeList) expr.evaluate(response, XPathConstants.NODESET);
        for (int i = 0; i < nodes.getLength(); i++) {
            values.add(nodes.item(i).getNodeValue());
        }

        assertEquals(1, values.size(), String.format("Value for id=%s not found in response.", id));
        return values.get(0);
    }
}
