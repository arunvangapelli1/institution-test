package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.opentest4j.AssertionFailedError;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.transact.sv.iso8583test.domain.authEngine.Account;

@TestInstance(Lifecycle.PER_CLASS)
class DtiFundTest extends DtiTestingBase {

    // txn amount needs to be 12 characters = $1.00
    private final String TXN_AMOUNT = "000000000100";

    @Test
    public void fundHappyPathTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiFundPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.FUND_RES_MTI, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.FUND_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_CH_BILLING));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertNotNull(DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));

        Account account = accountHolder.getAccounts().stream().filter(a -> ACCT_TYPE_ID1.equals(a.getAccountTypeId()))
                .findFirst().get();
        // updates balance to new expected balance after fund
        account.setBalance(account.getBalance().add(BigDecimal.ONE));
        assertTrue(DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION)
                .contains(getExpectedBalanceString(account)));
    }

    @Test
    public void fundPosAdapterFailureTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiFundPayloadConstructor(
                DtiPayload.builder().isoCredentialId("INVALIDPAN").isoTerminalKey(tResolver.getIso8583terminalNumber())
                        .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.FUND_RES_MTI, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.FUND_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        // validates value not in response
        assertThrows(AssertionFailedError.class,
                () -> DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_AMOUNT_CH_BILLING));
        assertEquals("INVALIDPAN", DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.FAILURE_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertThrows(AssertionFailedError.class,
                () -> DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }
}
