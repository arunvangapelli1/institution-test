package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.transact.sv.iso8583test.domain.authEngine.Account;
import com.transact.sv.iso8583test.domain.authEngine.AccountHolder;
import com.transact.sv.iso8583test.domain.resolver.Credential;
import com.transact.sv.iso8583test.domain.rulesEngine.Institution;

@TestInstance(Lifecycle.PER_CLASS)
class DtiBalanceInquiryTest extends DtiTestingBase {

    @Test
    public void balanceHappyPathTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiBalancePayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.BALANCE_RES_MTI,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.BALANCE_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
        List<Account> accounts = accountHolder.getAccounts();
        assertTrue(DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION)
                .contains(getExpectedBalanceString(accounts.get(0))));
        assertTrue(DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION)
                .contains(getExpectedBalanceString(accounts.get(1))));
        assertEquals(1, StringUtils.countMatches(
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION), BALANCE_INQ_SEPERATOR));
    }

    @Test
    public void balanceSingleAccountTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        Institution inst = rulesEngine.getInstitutions().get(0);
        AccountHolder ah = createAccountHolderWithNumAccounts(inst.getId(), 1);
        Credential cred = createGenericCredential(ah);
        try {
            String request = DtiUtility.dtiBalancePayloadConstructor(DtiPayload.builder()
                    .isoCredentialId(cred.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                    .isoMerchantKey(mResolver.getIso8583merchantNumber()).build());
            String response = sendDtiPayloadThenGetResponse(request);
            assertNotNull(response);

            Document responseDoc = DtiUtility.transformResponse(response);
            assertEquals(DtiUtility.BALANCE_RES_MTI,
                    DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
            assertEquals(DtiUtility.BALANCE_PROCESSING_CODE,
                    DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
            assertEquals(cred.getValue(), DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
            assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                    DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
            assertEquals(tResolver.getIso8583terminalNumber(),
                    DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
            assertEquals(mResolver.getIso8583merchantNumber(),
                    DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
            List<Account> accounts = ah.getAccounts();
            assertTrue(DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION)
                    .contains(getExpectedBalanceString(accounts.get(0))));
            assertEquals(0,
                    StringUtils.countMatches(
                            DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_BALANCE_INFORMATION),
                            BALANCE_INQ_SEPERATOR));
        } finally {
            aeAgent.deleteAccountHolder(ah.getAccountHolderId(), inst.getId());
            csAgent.deleteCredential(cred.getId());
        }
    }

    @Test
    public void balancePosAdapterFailureTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        String request = DtiUtility.dtiBalancePayloadConstructor(
                DtiPayload.builder().isoCredentialId("INVALIDPAN").isoTerminalKey(tResolver.getIso8583terminalNumber())
                        .isoMerchantKey(mResolver.getIso8583merchantNumber()).build());
        String response = sendDtiPayloadThenGetResponse(request);
        assertNotNull(response);

        Document responseDoc = DtiUtility.transformResponse(response);
        assertEquals(DtiUtility.BALANCE_RES_MTI,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.BALANCE_PROCESSING_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals("INVALIDPAN", DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.FAILURE_RESULT_CODE,
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(responseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }
}
