package com.transact.sv.iso8583test.dti;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transact.sv.iso8583test.agent.AuthEngineAgent;
import com.transact.sv.iso8583test.agent.CredentialServiceAgent;
import com.transact.sv.iso8583test.agent.ResolverAgent;
import com.transact.sv.iso8583test.agent.RulesEngineAgent;
import com.transact.sv.iso8583test.configuration.PosAdapterProperties;
import com.transact.sv.iso8583test.domain.authEngine.Account;
import com.transact.sv.iso8583test.domain.authEngine.AccountHolder;
import com.transact.sv.iso8583test.domain.resolver.Credential;
import com.transact.sv.iso8583test.domain.resolver.MerchantResolver;
import com.transact.sv.iso8583test.domain.resolver.TerminalResolver;
import com.transact.sv.iso8583test.domain.rulesEngine.AccountTypeRules;
import com.transact.sv.iso8583test.domain.rulesEngine.DrainOrder;
import com.transact.sv.iso8583test.domain.rulesEngine.Institution;
import com.transact.sv.iso8583test.domain.rulesEngine.Merchant;
import com.transact.sv.iso8583test.domain.rulesEngine.RulesEngine;
import com.transact.sv.iso8583test.domain.rulesEngine.TenderRule;
import com.transact.sv.iso8583test.domain.rulesEngine.Terminal;

@SpringBootTest
@ActiveProfiles
public class DtiTestingBase {

    @Autowired
    protected RulesEngineAgent reAgent;
    @Autowired
    protected AuthEngineAgent aeAgent;
    @Autowired
    protected ResolverAgent rAgent;
    @Autowired
    protected CredentialServiceAgent csAgent;

    @Autowired
    private PosAdapterProperties posAdapterProps;

    protected Socket client;
    protected BufferedReader in;
    protected PrintWriter out;

    protected RulesEngine rulesEngine;
    protected AccountHolder accountHolder;
    protected MerchantResolver mResolver;
    protected TerminalResolver tResolver;
    protected Credential credential;

    protected final String ISO_END_IND = "</isomsg>";
    private final String POS_TEST_NAME = "Pos Adapter Test ";
    protected String ACCT_TYPE_ID1 = UUID.randomUUID().toString();
    private String ACCT_TYPE_ID2 = UUID.randomUUID().toString();

    private boolean logDataSetup = false;
    private final String PRINT_SEPERATOR = "####################";

    private final String BALANCE_INQ_STRING = "%s:%s";
    protected final String BALANCE_INQ_SEPERATOR = "^";

    @BeforeAll
    protected void dataSetup() throws JsonProcessingException {
        rulesEngine = createGenericRulesEngine();
        Institution institution = rulesEngine.getInstitutions().get(0);
        accountHolder = createGenericAccountHolder(institution.getId());
        credential = createGenericCredential(accountHolder);
        Merchant merch = rulesEngine.getMerchants().get(0);
        mResolver = createGenericMerchantResolver(merch.getId(), institution.getId());
        tResolver = createGenericTerminalResolver(merch.getTerminals().get(0).getId());
        if (logDataSetup) {
            outputJson("RuleEngine Entity:", rulesEngine);
            outputJson("AccountHolder Entity:", accountHolder);
            outputJson("Credential Entity:", credential);
            outputJson("Merchant Resolver Entity:", mResolver);
            outputJson("Terminal Resolver Entity:", tResolver);
        }
    }

    @BeforeEach
    protected void socketConnectionSetup() throws UnknownHostException, IOException {
        client = new Socket(posAdapterProps.getHost(), posAdapterProps.getPort());
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        out = new PrintWriter(client.getOutputStream(), true);
    }

    @AfterEach
    protected void socketConnectionTeardown() throws IOException {
        out.close();
        in.close();
        client.close();
    }

    @AfterAll
    protected void dataCleanup() {
        cleanup(rulesEngine.getId(), accountHolder.getAccountHolderId(), rulesEngine.getInstitutions().get(0).getId(),
                mResolver.getMerchantKey(), tResolver.getTerminalKey(), credential.getId());
    }

    protected String getExpectedBalanceString(Account acct) {
        return String.format(BALANCE_INQ_STRING, acct.getAccountTypeId(), acct.getBalance().setScale(2));
    }

    protected String sendDtiPayloadThenGetResponse(String payload) throws IOException {
        sendPayload(payload);
        return readDtiResponse();
    }

    private void sendPayload(String payload) {
        outputXml("POS Adapter Incoming Payload:", payload);
        out.println(payload);
    }

    private String readDtiResponse() throws IOException {
        String inputLine;
        StringBuilder response = new StringBuilder();
        // TODO handle/fail invalid/no response to avoid infinite loop
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine.trim());
            if (inputLine.contains(ISO_END_IND)) {
                break;
            }
        }
        outputXml("POS Adapter Outgoing Payload:", response.toString());
        return response.toString();
    }

    private void outputXml(String label, String xml) {
        System.out.println(PRINT_SEPERATOR);
        System.out.println(label);
        System.out.println(PRINT_SEPERATOR);
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(new StringWriter());
            transformer.transform(new StreamSource(new StringReader(xml)), result);
            String xmlString = result.getWriter().toString();
            System.out.println(xmlString);
        } catch (Exception ex) {
            System.out.println(ex.getStackTrace());
        }
    }

    private void outputJson(String label, Object obj) {
        System.out.println(PRINT_SEPERATOR);
        System.out.println(label);
        System.out.println(PRINT_SEPERATOR);
        try {
            System.out.println(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(obj));
        } catch (Exception ex) {
            System.out.println(ex.getStackTrace());
        }
    }

    protected RulesEngine createGenericRulesEngine() {
        return reAgent.createRulesEngine(RulesEngine.builder().merchants(List.of(createMerchant()))
                .institutions(List.of(createInstitution())).build());
    }

    private Merchant createMerchant() {
        return Merchant.builder().name(POS_TEST_NAME + UUID.randomUUID().toString()).expireTimeToLive(60)
                .terminals(List.of(createTerminal())).build();
    }

    private Terminal createTerminal() {
        return Terminal.builder().name(POS_TEST_NAME + UUID.randomUUID().toString()).active(true).build();
    }

    private Institution createInstitution() {
        String tenderRuleId = UUID.randomUUID().toString();
        return Institution.builder().name(POS_TEST_NAME + UUID.randomUUID().toString())
                .defaultTenderRuleId(tenderRuleId).tenderRule(createTenderRule(tenderRuleId))
                .iso8583FundAccountType(ACCT_TYPE_ID1).build();
    }

    private TenderRule createTenderRule(String tenderRuleId) {
        return TenderRule.builder().id(tenderRuleId).name(POS_TEST_NAME + UUID.randomUUID().toString())
                .accountsDrainOrder(List.of(createDrainOrder(ACCT_TYPE_ID1), createDrainOrder(ACCT_TYPE_ID2))).build();
    }

    private DrainOrder createDrainOrder(String accountTypeID) {
        return DrainOrder.builder().accountTypeId(accountTypeID)
                .accountTypeRules(AccountTypeRules.builder().maxAmount(BigDecimal.valueOf(50.0)).build()).build();
    }

    protected AccountHolder createGenericAccountHolder(String institutionId) {
        return aeAgent.createAccountHolder(AccountHolder.builder().institutionId(institutionId)
                .accounts(List.of(createGenericAccount(ACCT_TYPE_ID1), createGenericAccount(ACCT_TYPE_ID2))).build());
    }

    protected AccountHolder createAccountHolderWithNumAccounts(String institutionId, int numAccounts) {
        List<Account> accts = new ArrayList<>();
        for (int i = 0; i < numAccounts; i++) {
            accts.add(createGenericAccount(UUID.randomUUID().toString()));
        }
        return aeAgent
                .createAccountHolder(AccountHolder.builder().institutionId(institutionId).accounts(accts).build());
    }

    private Account createGenericAccount(String accountTypeId) {
        return Account.builder().accountTypeId(accountTypeId).active(true).balance(BigDecimal.valueOf(150.0)).build();
    }

    protected MerchantResolver createGenericMerchantResolver(String merchantId, String instId) {
        return rAgent.createMerchantResolver(MerchantResolver.builder().merchantId(merchantId)
                .iso8583merchantNumber(String.valueOf(new Random().nextInt(1000000)))
                .merchantKey(UUID.randomUUID().toString()).institutionId(instId).build());
    }

    protected TerminalResolver createGenericTerminalResolver(String terminalId) {
        return rAgent.createTerminalResolver(TerminalResolver.builder().terminalId(terminalId)
                .iso8583terminalNumber(String.valueOf(new Random().nextInt(1000000)))
                .terminalKey(UUID.randomUUID().toString()).build());
    }

    protected Credential createGenericCredential(AccountHolder ah) {
        return csAgent.createCredential(Credential.builder().accountHolderId(ah.getAccountHolderId())
                .accountHolderInstitutionId(ah.getInstitutionId()).value(String.valueOf(new Random().nextInt(1000000)))
                .build());
    }

    protected void cleanup(String reId, String ahId, String instId, String mResolverKey, String tResolverKey,
            String credentialId) {
        rAgent.deleteMerchantResolver(mResolverKey);
        rAgent.deleteTerminalResolver(tResolverKey);
        reAgent.deleteRulesEngine(reId);
        aeAgent.deleteAccountHolder(ahId, instId);
        csAgent.deleteCredential(credentialId);
    }
}
