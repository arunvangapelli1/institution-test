package com.transact.sv.iso8583test.dti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@TestInstance(Lifecycle.PER_CLASS)
class DtiCaptureTest extends DtiTestingBase {

    // txn amount needs to be 12 characters = $1.00
    private final String TXN_AMOUNT = "000000000100";

    @Test
    public void captureHappyPathTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        
        String authRequest = DtiUtility.dtiAuthPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        
        String authResponse = sendDtiPayloadThenGetResponse(authRequest);
        assertNotNull(authResponse);
        Document authResponseDoc = DtiUtility.transformResponse(authResponse);

        // Verifying Authorization is valid.
        assertEquals(DtiUtility.AUTH_RES_MTI,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.AUTH_PROCESSING_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));

        // Get authCode from Authorization transaction's response.
        String authCode = DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM);
        // Fields required: 0,3,4,7,11,12,13,22,25,35,38,41,42,60
        String captureRequest = DtiUtility.dtiCapturePayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).authCode(authCode).build());

        String captureResponse = sendDtiPayloadThenGetResponse(captureRequest);
        assertNotNull(captureResponse);
        Document captureResponseDoc = DtiUtility.transformResponse(captureResponse);
        
        assertEquals(DtiUtility.CAPTURE_RES_MTI,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.CAPTURE_PROCESSING_CODE,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_AMOUNT_CH_BILLING));
        assertEquals(authCode, DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }

    @Test
    public void captureAuthCodeNotFoundTest()
            throws IOException, XPathExpressionException, SAXException, ParserConfigurationException {
        
        String authRequest = DtiUtility.dtiAuthPayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).build());
        
        String authResponse = sendDtiPayloadThenGetResponse(authRequest);
        assertNotNull(authResponse);
        Document authResponseDoc = DtiUtility.transformResponse(authResponse);

        // Verifying Authorization is valid.
        assertEquals(DtiUtility.AUTH_RES_MTI,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.AUTH_PROCESSING_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(credential.getValue(), DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CREDENTIAL_ID));
        assertEquals(DtiUtility.SUCCESS_RESULT_CODE,
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(authResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));

        // Set random authCode.
        String authCode = UUID.randomUUID().toString();
        String captureRequest = DtiUtility.dtiCapturePayloadConstructor(DtiPayload.builder()
                .isoCredentialId(credential.getValue()).isoTerminalKey(tResolver.getIso8583terminalNumber())
                .isoMerchantKey(mResolver.getIso8583merchantNumber()).txnAmount(TXN_AMOUNT).authCode(authCode).build());
        
        String captureResponse = sendDtiPayloadThenGetResponse(captureRequest);
        assertNotNull(captureResponse);
        Document captureResponseDoc = DtiUtility.transformResponse(captureResponse);
        
        assertEquals(DtiUtility.CAPTURE_RES_MTI,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_MESSAGE_TYPE_IND));
        assertEquals(DtiUtility.CAPTURE_PROCESSING_CODE,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_PROCESSING_CODE));
        assertEquals(TXN_AMOUNT, DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_AMOUNT_TRANSACTION));
        assertEquals(authCode, DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_RETRIEVAL_REFERENCE_NUM));
        assertEquals(DtiUtility.FAILURE_RESULT_CODE,
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_RESULT_CODE));
        assertEquals(tResolver.getIso8583terminalNumber(),
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_TERMINAL_ID));
        assertEquals(mResolver.getIso8583merchantNumber(),
                DtiUtility.getFieldValue(captureResponseDoc, DtiUtility.FIELD_CARD_ACCEPTOR_ID));
    }
}
