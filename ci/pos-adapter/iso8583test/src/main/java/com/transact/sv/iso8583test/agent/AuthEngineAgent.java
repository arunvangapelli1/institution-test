package com.transact.sv.iso8583test.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.transact.sv.iso8583test.configuration.AuthEngineProperties;
import com.transact.sv.iso8583test.domain.authEngine.AccountHolder;

@Service
public class AuthEngineAgent {

    private static final String INSTITUTION_ID = "INSTITUTION-ID";

    @Autowired
    private AuthEngineProperties aeProps;

    @Autowired
    private RestTemplate restTemplate;

    public AccountHolder createAccountHolder(AccountHolder request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(INSTITUTION_ID, request.getInstitutionId());

        return restTemplate.postForEntity(aeProps.getDomain() + aeProps.getContext(),
                new HttpEntity<>(request, headers), AccountHolder.class).getBody();
    }

    public void deleteAccountHolder(String accountHolderId, String instId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(INSTITUTION_ID, instId);
        restTemplate.exchange(aeProps.getDomain() + aeProps.getContext() + accountHolderId, HttpMethod.DELETE,
                new HttpEntity<Object>(headers), void.class, 1);
    }
}
