package com.transact.sv.iso8583test.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "terminal-resolver")
public class TerminalResolverProperties extends AgentProperties {
}
