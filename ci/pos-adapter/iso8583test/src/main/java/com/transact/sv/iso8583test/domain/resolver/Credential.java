package com.transact.sv.iso8583test.domain.resolver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Credential {
    private String id;
    private String accountHolderId;
    private String accountHolderInstitutionId;
    @Builder.Default
    private String type = "SAMPLE_MAGSTRIPE";
    private String value;
    @Builder.Default
    private boolean enabled = true;
}
