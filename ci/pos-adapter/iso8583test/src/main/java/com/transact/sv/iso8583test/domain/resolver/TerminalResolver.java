package com.transact.sv.iso8583test.domain.resolver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TerminalResolver {
    private String terminalId;
    private String iso8583terminalNumber;
    private String terminalKey;
}
