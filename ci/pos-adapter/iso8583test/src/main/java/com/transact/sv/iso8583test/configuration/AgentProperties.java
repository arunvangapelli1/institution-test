package com.transact.sv.iso8583test.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AgentProperties {
    private String domain;
    private String context;

}
