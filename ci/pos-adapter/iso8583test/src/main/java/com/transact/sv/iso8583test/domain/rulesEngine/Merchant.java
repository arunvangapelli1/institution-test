package com.transact.sv.iso8583test.domain.rulesEngine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Merchant {
    private String id;
    private String rulesEngineId;
    @Builder.Default
    private String docVersion = "1.0";
    private String name;
    private int expireTimeToLive;
    private String modifiedDateTime;
    @Singular
    private List<Terminal> terminals;
}