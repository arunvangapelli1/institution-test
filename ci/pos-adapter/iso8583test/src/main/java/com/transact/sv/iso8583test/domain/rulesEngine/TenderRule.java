package com.transact.sv.iso8583test.domain.rulesEngine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TenderRule {
    private String id;
    private String name;
    private String modifiedDateTime;
    private List<DrainOrder> accountsDrainOrder;
}
