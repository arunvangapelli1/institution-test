package com.transact.sv.iso8583test.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.transact.sv.iso8583test.configuration.MerchantResolverProperties;
import com.transact.sv.iso8583test.configuration.TerminalResolverProperties;
import com.transact.sv.iso8583test.domain.resolver.MerchantResolver;
import com.transact.sv.iso8583test.domain.resolver.TerminalResolver;

@Service
public class ResolverAgent {

    @Autowired
    private MerchantResolverProperties merchantResolverProps;

    @Autowired
    private TerminalResolverProperties terminalResolverProps;

    @Autowired
    private RestTemplate restTemplate;
    public MerchantResolver createMerchantResolver(MerchantResolver request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return restTemplate.postForEntity(merchantResolverProps.getDomain() + merchantResolverProps.getContext(),
                new HttpEntity<>(request, headers), MerchantResolver.class).getBody();
    }

    public void deleteMerchantResolver(String resolverKey) {
        restTemplate.delete(merchantResolverProps.getDomain() + merchantResolverProps.getContext() + resolverKey);
    }

    public TerminalResolver createTerminalResolver(TerminalResolver request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return restTemplate.postForEntity(terminalResolverProps.getDomain() + terminalResolverProps.getContext(),
                new HttpEntity<>(request, headers), TerminalResolver.class).getBody();
    }

    public void deleteTerminalResolver(String resolverKey) {
        restTemplate.delete(terminalResolverProps.getDomain() + terminalResolverProps.getContext() + resolverKey);
    }
}
