package com.transact.sv.iso8583test.domain.rulesEngine;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Terminal {
    private String id;
    private String name;
    private Boolean active;
    private String modifiedDateTime;
}
