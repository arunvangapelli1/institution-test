package com.transact.sv.iso8583test.domain.rulesEngine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RulesEngine {
    private String id;
    private List<Merchant> merchants;
    private List<Institution> institutions;
}
