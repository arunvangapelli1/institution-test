package com.transact.sv.iso8583test.domain.authEngine;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account {
    private Long id;
    private String accountId;
    private String type;
    private Boolean active;
    @Builder.Default
    private Long authCount = 0L;
    private String accountTypeId;
    private BigDecimal balance;
    private BigDecimal openToBuyBalance;
    private BigDecimal creditLimit;
    private Long version;
}
