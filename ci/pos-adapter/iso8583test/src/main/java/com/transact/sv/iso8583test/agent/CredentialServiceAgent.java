package com.transact.sv.iso8583test.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.transact.sv.iso8583test.configuration.CredentialServiceProperties;
import com.transact.sv.iso8583test.domain.resolver.Credential;

@Service
public class CredentialServiceAgent {

    @Autowired
    private CredentialServiceProperties csProps;

    @Autowired
    private RestTemplate restTemplate;

    public Credential createCredential(Credential request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return restTemplate.postForEntity(csProps.getDomain() + csProps.getContext(),
                new HttpEntity<>(request, headers), Credential.class).getBody();
    }

    public void deleteCredential(String credentialId) {
        restTemplate.delete(csProps.getDomain() + csProps.getContext() + credentialId, HttpMethod.DELETE);
    }
}
