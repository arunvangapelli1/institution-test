package com.transact.sv.iso8583test.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "pos-adapter-connection")
@Getter
@Setter
@NoArgsConstructor
public class PosAdapterProperties {
    private String host;
    private int port;
}
