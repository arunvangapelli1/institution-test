package com.transact.sv.iso8583test.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.transact.sv.iso8583test.configuration.RulesEngineProperties;
import com.transact.sv.iso8583test.domain.rulesEngine.RulesEngine;

@Service
public class RulesEngineAgent {

    @Autowired
    private RulesEngineProperties reProps;

    @Autowired
    private RestTemplate restTemplate;

    public RulesEngine createRulesEngine(RulesEngine request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return restTemplate.postForEntity(reProps.getDomain() + reProps.getContext(),
                new HttpEntity<>(request, headers), RulesEngine.class).getBody();
    }

    public void deleteRulesEngine(String rulesEngineId) {
        restTemplate.delete(reProps.getDomain() + reProps.getContext() + rulesEngineId);
    }
}
