package com.transact.sv.iso8583test.domain.authEngine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountHolder {
    private Long id;
    @Builder.Default
    private String docversion = "1.0";
    private String accountHolderId;
    private Boolean active;
    private String institutionId;
    private List<Account> accounts;
    private Long version;
}
