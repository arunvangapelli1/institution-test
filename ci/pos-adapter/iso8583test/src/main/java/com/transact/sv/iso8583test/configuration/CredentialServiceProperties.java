package com.transact.sv.iso8583test.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "credential-service")
public class CredentialServiceProperties extends AgentProperties {
}
