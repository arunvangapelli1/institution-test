package com.transact.sv.iso8583test.domain.resolver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MerchantResolver {
    private String merchantId;
    private String iso8583merchantNumber;
    private String merchantKey;
    private String institutionId;
}
