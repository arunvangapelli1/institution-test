package com.transact.sv.iso8583test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Iso8583testApplication {
    public static void main(String[] args) {
        SpringApplication.run(Iso8583testApplication.class, args);
    }
}
