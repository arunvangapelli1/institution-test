package com.transact.sv.iso8583test.domain.rulesEngine;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Institution {
    private String id;
    @Builder.Default
    private String docVersion = "1.0";
    private String rulesEngineId;
    private String name;
    private String defaultTenderRuleId;
    private String iso8583FundAccountType;
    @Singular
    private List<TenderRule> tenderRules;
}