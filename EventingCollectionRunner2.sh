#!/bin/bash

#import set up
. setup.sh $1

runEventingCollection "ts-sv-api-accountholder-eventing"
runEventingCollection "ts-sv-api-institution-eventing"
runEventingCollection "ts-sv-api-tenderrule-eventing"
runEventingCollection "ts-sv-api-terminal-eventing"

#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
