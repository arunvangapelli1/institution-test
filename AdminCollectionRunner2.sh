#!/bin/bash

#import set up
. setup.sh $1

runAdminCollection "ts-sv-api-limits"
runAdminCollection "ts-sv-api-merchant"
runAdminCollection "ts-sv-api-tenderrule"

#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
