#!/bin/bash

#import set up
. setup.sh $1

runEventingCollection "ts-sv-api-merchant-eventing"
runEventingCollection "ts-sv-api-limits-eventing"
runEventingCollection "ts-sv-api-accounts-eventing"

#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
