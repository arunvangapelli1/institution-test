#!/bin/sh

# stop on first error

set +e;
error=false
error_count=0
typeset -a passedArray
typeset -a failedArray

STORED_VALUE_ENV_FILE="./ci/variables/stored-value-$1.postman_environment.json"
STORED_VALUE_GLOBALS_FILE="./ci/variables/stored-value.postman_globals.json"

function appendArray {
  if [ "$error" = "true" ]; then
    failedArray["${#failedArray[@]}"]=$1
  else
    passedArray["${#passedArray[@]}"]=$1
  fi
}

function printPassedSuites {
  echo ""
  echo ""
  for i in "${passedArray[@]}"
  do
    echo "$i Test Suite Succesfully Passed"
  done
}

function printfailedSuites {
  echo ""
  echo ""
  for i in "${failedArray[@]}"
  do
    echo "$i Test Suite Failed"
  done
}

function resetValues {
  let "error_count++"
  error=false
}

function runOrchestratorCollection {
  set +e
  #docker run -v $(pwd):/etc/newman postman/newman_ubuntu1404 run "./ci/postman/orchestrator/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="orchestrator-$1-results.html" || error=true
  newman run "./ci/postman/orchestrator/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="orchestrator-$1-results.html" || error=true
  appendArray "$1"
  if [ "$error" = "true" ]; then
    resetValues
  fi
}

function runIso8583 {
  set +e
  docker run -v $(pwd):/etc/newman postman/newman_ubuntu1404 run "./ci/postman/orchestrator/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE  -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="iso8583-$1-results.html" || error=true
  appendArray "$1"
  if [ "$error" = "true" ]; then
    resetValues
  fi
}

function runAdminCollection {
  set +e
  #docker run -v $(pwd):/etc/newman postman/newman_ubuntu1404 run "./ci/postman/admin/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE  -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="$1-results.html" || error=true
  newman run "./ci/postman/admin/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE  -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="$1-results.html" || error=true
  appendArray "$1"
  if [ "$error" = "true" ]; then
    resetValues
  fi
}

function runEventingCollection {
  set +e
  docker run -v $(pwd):/etc/newman postman/newman_ubuntu1404 run "./ci/postman/eventing/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE  -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="$1-results.html" || error=true
  appendArray "$1"
  if [ "$error" = "true" ]; then
    resetValues
  fi
}

function runResolverCollection {
  set +e
  docker run -v $(pwd):/etc/newman postman/newman_ubuntu1404 run "./ci/postman/resolver/$1.postman_collection.json" --environment=$STORED_VALUE_ENV_FILE   -g $STORED_VALUE_GLOBALS_FILE --reporters="html,cli" --reporter-html-export="$1-results.html" || error=true
  appendArray "$1"
  if [ "$error" = "true" ]; then
    resetValues
  fi
}

function onExit {
  if [ $error_count -gt 0 ]; then
      exit 1;
  else
      echo "All Test have passed";
  fi
}

trap onExit EXIT;
