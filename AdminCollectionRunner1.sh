#!/bin/bash

#import set up
. setup.sh $1

runAdminCollection "ts-sv-api-accountholder"
runAdminCollection "ts-sv-api-accounttype"
runAdminCollection "ts-sv-api-institution"

#Error Logging for Passed and Failed Test Suites
printPassedSuites
printfailedSuites
